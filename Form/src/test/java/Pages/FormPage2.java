package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FormPage2 extends BasePage {
    public FormPage2(WebDriver driver) {
        super(driver);
    }

    public FormPage2 enterFirstName(){
        WebElement firstNameField = driver.findElement(By.id("firstname"));
        firstNameField.sendKeys("Alex");
        return this;
    }

    public FormPage2 enterLastName(){
        WebElement lastNameField = driver.findElement(By.id("lastname"));
        lastNameField.sendKeys("Jacks");
        return this;
    }

    public FormPage2 enterDateOfBirth(){
        WebElement dateOfBirthField = driver.findElement(By.id("birthdate"));
        dateOfBirthField.sendKeys("18/09/1984");
        return this;
    }

    public FormPage2 selectGender(){
        WebElement genderMaleRadio = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div/form/div/section[2]/div[4]/p/label[1]"));
        genderMaleRadio.click();
        return this;
    }

    public FormPage2 enterStreetAddress(){
        WebElement streetAddressField = driver.findElement(By.id("streetaddress"));
        streetAddressField.sendKeys("Some random address");
        return this;
    }

    public FormPage2 enterCountry(){
        WebElement countryField = driver.findElement(By.id("country"));
        Select select = new Select(countryField);
        select.selectByVisibleText("Brazil");
        return this;
    }

    public FormPage2 enterZipCode(){
        WebElement zipCodeField = driver.findElement(By.id("zipcode"));
        zipCodeField.sendKeys("43256200");
        return this;
    }

    public FormPage2 enterCity(){
        WebElement cityField = driver.findElement(By.id("city"));
        cityField.sendKeys("Salvador");
        return this;
    }

    public FormPage2 enterOccupation(){
        WebElement occupationField = driver.findElement(By.id("occupation"));
        Select select = new Select(occupationField);
        select.selectByVisibleText("Employee");
        return this;
    }

    public FormPage2 selectHobbies(){
        WebElement hobbieSpeeding = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div/form/div/section[2]/div[10]/p/label[1]/span"));
        hobbieSpeeding.click();
        return this;
    }

    public FormPage2 enterWebSite(){
        WebElement websiteField = driver.findElement(By.id("website"));
        websiteField.sendKeys("Some random website");
        return this;
    }

    public FormPage3 clickOnNextButton(){
        WebElement nextButton = driver.findElement(By.id("nextenterproductdata"));
        nextButton.click();
        return new FormPage3(driver);
    }
}
