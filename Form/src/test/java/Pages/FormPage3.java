package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FormPage3 extends BasePage {
    public FormPage3(WebDriver driver) {
        super(driver);
    }

    public FormPage3 enterStartDate(){
        WebElement startDateField = driver.findElement(By.id("startdate"));
        startDateField.sendKeys("14/02/2021");
        return this;
    }

    public FormPage3 selectInsurance(){
        WebElement insurance = driver.findElement(By.id("insurancesum"));
        Select select = new Select(insurance);
        select.selectByVisibleText("10.000.000,00");
        return this;
    }

    public FormPage3 selectMeritRating(){
        WebElement merit = driver.findElement(By.id("meritrating"));
        Select select = new Select(merit);
        select.selectByVisibleText("Bonus 1");
        return this;
    }

    public FormPage3 selectDamageInsurance(){
        WebElement dmgInsurance = driver.findElement(By.id("damageinsurance"));
        Select select = new Select(dmgInsurance);
        select.selectByVisibleText("Partial Coverage");
        return this;
    }

    public FormPage3 selectEuroProtection(){
        WebElement euroProtecField = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div/form/div/section[3]/div[5]/p/label[1]/span"));
        euroProtecField.click();
        return this;
    }

    public FormPage3 selectCourtesy(){
        WebElement courtesyField = driver.findElement(By.id("courtesycar"));
        Select select = new Select(courtesyField);
        select.selectByVisibleText("No");
        return this;
    }

    public FormPage4 clickOnNextButton(){
        WebElement nextButton = driver.findElement(By.id("nextselectpriceoption"));
        nextButton.click();
        return new FormPage4(driver);
    }



}
