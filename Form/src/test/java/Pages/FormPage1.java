package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;




public class FormPage1 {

    private WebDriver driver;

    public FormPage1(WebDriver driver){
        this.driver = driver;
    }

    public FormPage1 selectMakeField (){
        WebElement makeField = driver.findElement(By.id("make"));
        Select select = new Select(makeField);
        select.selectByVisibleText("Suzuki");
        return this;
    }

    public FormPage1 selectModelField(){
        WebElement modelField = driver.findElement(By.id("model"));
        Select select = new Select(modelField);
        select.selectByVisibleText("Motorcycle");
        return this;
    }

    public FormPage1 enterCylinderCapacity(){
        WebElement cylinderCapField = driver.findElement(By.id("cylindercapacity"));
        cylinderCapField.sendKeys("300");
        return this;
    }

    public FormPage1 enterEnginePerformance(){
        WebElement enginePerformanceField = driver.findElement(By.id("engineperformance"));
        enginePerformanceField.sendKeys("200");
        return this;
    }

    public FormPage1 enterDateOfManufacture(){
        WebElement dateManufactField = driver.findElement(By.id("dateofmanufacture"));
        dateManufactField.sendKeys("12/05/2020");
        return this;
    }

    public FormPage1 enterNumberOfSeats(){
        WebElement numberOfSeatsField = driver.findElement(By.id("numberofseats"));
        Select select = new Select(numberOfSeatsField);
        select.selectByVisibleText("1");
        return this;
    }

    public FormPage1 selectRightHandDriverYesOption(){
        WebElement rightHandOptionYes = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div/form/div/section[1]/div[7]/p/label[1]/span"));
        rightHandOptionYes.click();
        return this;
    }

    public FormPage1 selectNumberOfSeatsMotorcycle(){
        WebElement numberOfSeatsMotorcycle = driver.findElement(By.id("numberofseatsmotorcycle"));
        Select select = new Select(numberOfSeatsMotorcycle);
        select.selectByVisibleText("1");
        return this;
    }

    public FormPage1 selectFuel(){
        WebElement selectFuelField = driver.findElement(By.id("fuel"));
        Select select = new Select(selectFuelField);
        select.selectByVisibleText("Diesel");
        return this;
    }

    public FormPage1 enterPayload(){
        WebElement payloadField = driver.findElement(By.id("payload"));
        payloadField.sendKeys("12000");
        return this;
    }

    public FormPage1 enterTotalWeight(){
        WebElement totalWeightField = driver.findElement(By.id("totalweight"));
        totalWeightField.sendKeys("1000kg");
        return this;
    }

    public FormPage1 enterListPrice(){
        WebElement listPriceField = driver.findElement(By.id("listprice"));
        listPriceField.sendKeys("Arimoganda");
        return this;
    }

    public FormPage1 enterLicencePlateNumber(){
        WebElement licencePlateField = driver.findElement(By.id("licenseplatenumber"));
        licencePlateField.sendKeys("Licence Number 9823");
        return this;
    }

    public FormPage1 enterAnualMileage(){
        WebElement anualMileageField = driver.findElement(By.id("annualmileage"));
        anualMileageField.sendKeys("2000");
        return this;
    }

    public FormPage2 clickOnNextButton(){
        WebElement nextButton1 = driver.findElement(By.id("nextenterinsurantdata"));
        nextButton1.click();
        return new FormPage2(driver);
    }


}
