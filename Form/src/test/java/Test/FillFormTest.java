package Test;

import Pages.FormPage1;
import Pages.FormPage2;
import Pages.FormPage3;
import basetest.Web;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FillFormTest {

    private WebDriver driver;

    @BeforeTest
    public void setUp(){
        driver = Web.cresteChrome();
    }

    @Test
    public void validateFormTest(){
        new FormPage1(driver)
                .selectMakeField()
                .selectModelField()
                .enterCylinderCapacity()
                .enterEnginePerformance()
                .enterDateOfManufacture()
                .enterNumberOfSeats()
                .selectRightHandDriverYesOption()
                .selectNumberOfSeatsMotorcycle()
                .selectFuel()
                .enterPayload()
                .enterTotalWeight()
                .enterListPrice()
                .enterLicencePlateNumber()
                .enterAnualMileage()
                .clickOnNextButton();

        new FormPage2(driver)
                .enterFirstName()
                .enterLastName()
                .enterDateOfBirth()
                .selectGender()
                .enterStreetAddress()
                .enterCountry()
                .enterOccupation()
                .selectHobbies()
                .enterWebSite()
                .clickOnNextButton();

        new FormPage3(driver)
                .enterStartDate()
                .selectInsurance()
                .selectMeritRating()
                .selectDamageInsurance()
                .selectEuroProtection()
                .selectCourtesy()
                .clickOnNextButton();
    }

    @AfterTest
    public void tearDown(){
        //driver.quit();
    }

}
